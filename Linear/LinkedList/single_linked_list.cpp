#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

typedef struct LNode {
    int data; // 数据域
    LNode *next; // 指针域
} LNode, *LinkList;

bool InitList(LinkList &L);

LinkList List_HeadInsert();

bool ListInsert(LinkList &L, int i, int e);

bool InsertPrior(LinkList &L, LNode *p, int e);

bool InsertNextNode(LinkList &L, LNode *p, int e);

bool ListDelete(LinkList &L, int i, int &e);

bool DeleteNode(LinkList &L, LNode *target);

LNode *GetElem(LinkList L, int i);

LNode *LocateElem(LinkList L, int e);

int GetLength(LinkList L);

bool Empty(LinkList L);

void PrintList(LinkList L);

/**
 * 初始化链表（带头节点）
 * @param L
 * @return
 */
bool InitList(LinkList &L) {
    L = (LNode *) malloc(sizeof(LNode));
    if (L == nullptr) {
        return false;
    }
    L->data = 0; // 头节点数据域用于存放链表长度
    L->next = nullptr;
    PrintList(L);
    return true;
}

/**
 * 头插法
 * @return
 */
LinkList List_HeadInsert() {
    LinkList L;
    InitList(L);

    int e;
    LNode *p = L;
    printf("【头插法】请输入数据：\n");

    while (cin >> e) {
        InsertPrior(L, p, e);
        p = L->next;
    }

    return L;
}

/**
 * 在第i个位置插入元素e
 * @param L
 * @param i
 * @param e
 * @return
 */
bool ListInsert(LinkList &L, int i, int e) {
    if (i < 1 || i > GetLength(L) + 1) {
        printf("【插入】非法位序i：%d\n", i);
        return false;
    }

    // 查找第i-1个节点
    LNode *t = GetElem(L, i - 1);
    if (t == nullptr) {
        printf("【插入】查找第%d的节点失败！\n", i - 1);
        return false;
    }

    printf("【插入】查找第%d节点 值：%d\t 指针：%p\n", i - 1, t->data, t);

    return InsertNextNode(L, t, e);
}

/**
 * 前插操作：在节点p前面插入元素e
 * @param L
 * @param p
 * @param e
 * @return
 */
bool InsertPrior(LinkList &L, LNode *p, int e) {
    if (p == nullptr) {
        printf("【InsertPrior】空节点p！\n");
    }

    // 由于p节点没有直接前驱，所以只能从头开始遍历，但随着问题规模n的增大，无疑是很费时的，O(n)

    // 偷天换日大法：

    LNode *n = (LNode *) malloc(sizeof(LNode));
    if (n == nullptr) {
        printf("【InsertPrior】申请内存失败！\n");
        return false;
    }

    // 采用后插法先插入节点
    n->next = p->next;
    p->next = n;

    // 交换节点数据位置
    n->data = p->data;
    p->data = e;
    L->data++;

    PrintList(L);
    return true;
}

/**
 * 后插操作：在节点p后面插入元素e
 * @param p
 * @param e
 * @return
 */
bool InsertNextNode(LinkList &L, LNode *p, int e) {
    LNode *n = (LNode *) malloc(sizeof(LNode));
    if (n == nullptr) {
        printf("【InsertNextNode】申请内存失败！\n");
        return false;
    }

    n->data = e;
    n->next = p->next;
    p->next = n;
    L->data++;

    PrintList(L);
    return true;
}

/**
 * 按位序删除节点
 * @param L
 * @param i
 * @param e
 * @return
 */
bool ListDelete(LinkList &L, int i, int &e) {
    if (i < 1 || i > GetLength(L)) {
        printf("【ListDelete】非法位序：%d\t", i);
        return false;
    }

    // 找到第i-1个节点
    LNode *pre = GetElem(L, i - 1);
    if (pre == nullptr || pre->next == nullptr) {
        printf("【ListDelete】找不到节点！\t");
        return false;
    }
    // 目标节点
    LNode *target = pre->next;
    e = target->data;
    pre->next = target->next;
    free(target);

    L->data--;
    PrintList(L);
    return true;
}

/**
 * 删除指定节点
 * @param L
 * @param p
 * @return
 */
bool DeleteNode(LinkList &L, LNode *target) {
    if (target == nullptr) {
        printf("【DeleteNode】空目标节点！\n");
        return false;
    }

    // 因为删除目标节点后，需要修改前一个节点的后驱指针，直接遍历的话太耗时了，O(n)

    // 还是偷天换日法：

    // 目标节点的下一个节点
    LNode *nextNode = target->next;
    if (nextNode == nullptr) { // 目标节点为最后一个节点，无力回天，只能遍历啦~
        // todo
        return false;
    }

    target->data = nextNode->data;
    target->next = nextNode->next;
    // 明明要释放的是target，现在变成释放nextNode，此为偷天换日
    free(nextNode);

    L->data--;

    PrintList(L);
    return true;
}

/**
 * 按位查找：获取位序为i的节点
 * @param L
 * @param i
 * @return
 */
LNode *GetElem(LinkList L, int i) {
    if (i < 0) {
        printf("【GetElem】非法位序：%d\n", i);
        return nullptr;
    }

    LNode *p = L;
    int j = 0;
    while (p->next != nullptr && j < i) {
        j++;
        p = p->next;
    }

    return p;
}

/**
 * 按值查找：获取元素值为e的节点
 * @param L
 * @param e
 * @return
 */
LNode *LocateElem(LinkList L, int e) {
    // 只能从头遍历查找，很鸡肋，O(n)
    LNode *cur = L->next;
    while (cur != nullptr && cur->data != e) {
        cur = cur->next;
    }
    return cur;
}

int GetLength(LinkList L) {
    return L->data;
}

/**
 * 判空
 * @param L
 * @return
 */
bool Empty(LinkList L) {
    // 不带头节点方式：
    //return L == nullptr;

    // 头节点存放链表长度时：
    //return GetLength(L) <= 0;

    // 带头节点方式：
    return L->next == nullptr;
}

void PrintList(LinkList L) {
    printf("------- 打印开始 -------\n");
    if (Empty(L)) {
        printf("空表！\n");
    }
    LNode *p = L->next;
    int i = 1;
    while (p != nullptr) {
        printf("位序：%d\t 值：%d\t 指针：%p\t 后驱指针：%p\n", i, p->data, p, p->next);
        i++;
        p = p->next;
    }
    printf("------- 打印结束 -------\n");
}

int main() {
    LinkList L;
    InitList(L);
    ListInsert(L, 1, 3);
    ListInsert(L, 1, 2);
    ListInsert(L, 3, 484);

    int DelElem;
    ListDelete(L, 3, DelElem);
    printf("删除位序为：%d的元素值为：%d\n", 2, DelElem);

    LNode *DelNode = GetElem(L, 1);
    bool DelRes = DeleteNode(L, DelNode);
    printf("删除位序为：%d的节点信息：\t v:%d\t p:%p\t res:%d\n", 1, DelNode->data, DelNode, DelRes);

    LNode *ElemNode = LocateElem(L, 3);


    if (ElemNode == nullptr) {
        printf("查找失败\n");
    } else {
        printf("查找的节点 值：%d\t 指针：%p \n", ElemNode->data, ElemNode);
    }


    // 头插法
    LinkList LH = List_HeadInsert();
    PrintList(LH);
    // todo 尾插法

    return 0;
}