
#include <stdio.h>
#include <stdlib.h>

#define InitLength 2

/**
 * 动态顺序表
 */
typedef struct {
    int *data;
    int MaxSize, length;
} SqList;


void InitList(SqList &L);

void IncreaseSize(SqList &L, int len);

bool ListInsert(SqList &L, int i, int e);

bool ListDelete(SqList &L, int i, int &e);

int GetElem(SqList L, int i);

int LocateElem(SqList L, int e);

void PrintList(SqList L);

int GetLength(SqList L);

bool Empty(SqList L);

void InitList(SqList &L) {
    L.data = (int *) malloc(sizeof(int) * InitLength);
    L.length = 0;
    L.MaxSize = InitLength;
}

void IncreaseSize(SqList &L, int len) {
    int NewMaxSize = L.MaxSize + len;
    int *p = L.data;
    L.data = (int *) malloc(sizeof(int) * NewMaxSize);
    for (int i = 0; i < L.length; i++) {
        L.data[i] = p[i];
    }
    free(p);
    L.MaxSize = NewMaxSize;
    PrintList(L);
}

bool ListInsert(SqList &L, int i, int e) {
    if (i < 1 || i > L.length + 1) {
        printf("【插入】非法位序：%d\n", i);
        return false;
    }
    if (L.length >= L.MaxSize) {
        IncreaseSize(L, L.length * 2);
    }
    for (int j = L.length; j >= i; j--) {
        L.data[j] = L.data[j - 1];
    }
    L.data[i - 1] = e;
    L.length++;
    PrintList(L);
    return true;
}

bool ListDelete(SqList &L, int i, int &e) {
    if (i < 1 || i > L.length) {
        printf("【删除】非法位序：%d\n", i);
        return false;
    }
    e = L.data[i - 1];
    for (int j = i; j < L.length; j++) {
        L.data[j - 1] = L.data[j];
    }
    L.length--;
    PrintList(L);
    return true;
}

/**
 * 按位序查找
 * @param L
 * @param i
 * @return
 */
int GetElem(SqList L, int i) {
    if (i < 1 || i > L.length) {
        printf("【按位序查找】非法位序：%d\n", i);
        return -1;
    }
    return L.data[i - 1];
}

/**
 * 按值查找元素
 * @param L
 * @param e
 * @return 位序i
 */
int LocateElem(SqList L, int e) {
    for (int i = 0; i < L.length; i++) {
        if (e == L.data[i]) {
            return i + 1;
        }
    }
    printf("【按值查找元素】找不到元素值为：%d的位序！\n", e);
    return 0;
}

void PrintList(SqList L) {
    printf("------- 开始 ------- \n");
    if (Empty(L)) {
        printf("空表！\n");
    }
    for (int i = 0; i < L.length; i++) {
        printf("位序：%d\t 值：%d\t 指针：%p \n", i + 1, L.data[i], &L.data[i]);
    }
    printf("长度：%d\t 最大长度：%d\t \n", L.length, L.MaxSize);
    printf("------- 结束 ------- \n");
}

int GetLength(SqList L) {
    return L.length;
}

bool Empty(SqList L) {
    return GetLength(L) <= 0;
}

int main() {
    SqList L;
    InitList(L);
    //IncreaseSize(L, 5);

    ListInsert(L, 1, 484);
    ListInsert(L, 1, 26574);
    ListInsert(L, 2, 87);

    int DelElem;
    ListDelete(L, 1, DelElem);
    printf("删除位序为：%d的元素值为：%d\n", 1, DelElem);

    LocateElem(L, 2333);

    int index = LocateElem(L, 87);
    printf("值为87的位序为：%d\n", index);

    int Elem = GetElem(L, 2);
    printf("位序为：%d的值为：%d\n", 2, Elem);

    return 0;
}