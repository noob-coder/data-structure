
#include <stdio.h>

#define MaxLength 10

/**
 * 静态顺序表
 */
typedef struct {
    int data[MaxLength];
    int length;
} SqList;

// 先声明函数

/**
 * 初始化
 * @param L
 */
void InitList(SqList &l);

bool ListInsert(SqList &L, int i, int e);

bool ListDelete(SqList &L, int i, int &e);

int GetElem(SqList L, int i);

/**
 * 获取表长
 * @param L
 * @return
 */
int GetLength(SqList L);

/**
 * 判空
 * @param L
 * @return true-空
 */
bool Empty(SqList L);

/**
 * 打印列表
 * @param L
 */
void PrintList(SqList L);


void InitList(SqList &L) {
    for (int i = 0; i < MaxLength; i++) {
        L.data[i] = 0;
    }
    L.length = 0;
}

/**
 * 插入
 * 最好的情况    O(1)
 * 最坏的情况    O(n)
 * 平均的情况    O(n)
 * @param L
 * @param i
 * @param e
 * @return true-成功
 */
bool ListInsert(SqList &L, int i, int e) {
    if (i < 1 || i > L.length + 1 || L.length >= MaxLength) {
        printf("【插入】非法位序：%d\n", i);
        return false;
    }
    for (int j = L.length; j >= i; j--) {
        L.data[j] = L.data[j - 1];
    }
    L.data[i - 1] = e;
    L.length++;
    PrintList(L);
    return true;
}

/**
 * 删除
 * 最好的情况    O(1)
 * 最坏的情况    O(n)
 * 平均的情况    O(n)
 * @param L
 * @param i
 * @param e
 * @return
 */
bool ListDelete(SqList &L, int i, int &e) {
    if (i < 1 || i > L.length) {
        printf("【删除】非法位序：%d\n", i);
        return false;
    }
    e = L.data[i - 1];
    for (int j = i; j <= L.length; j++) {
        L.data[j - 1] = L.data[j];
    }
    L.length--;
    PrintList(L);
    return true;
}

/**
 * 按位查找     O(1)
 * 随机存取
 * @return
 */
int GetElem(SqList L, int i) {
    if (i < 1 || i > L.length) {
        printf("【按位查找】非法位序：%d\n", i);
        return -1;
    }
    return L.data[i - 1];
}

/**
 * 按值查找     O(n)
 * @param L
 * @param e
 * @return i位序
 */
int LocateElem(SqList L, int e) {
    for (int i = 0; i < L.length; i++) {
        if (e == L.data[i]) {
            return i + 1;
        }
    }
    return 0;
}

int GetLength(SqList L) {
    return L.length;
}

bool Empty(SqList L) {
    //return (bool) GetLength(L);
    return GetLength(L) <= 0;
}

void PrintList(SqList L) {
    if (Empty(L)) {
        printf("空表！\n");
    }
    for (int i = 0; i < L.length; i++) {
        printf("位序：%d\t 值：%d\t 指针：%p\n", i + 1, L.data[i], &L.data[i]);
    }
    printf("长度：%d\t 长度指针：%p\n", L.length, &L.length);
}

int main() {
    SqList L;
    InitList(L);
    PrintList(L);
    ListInsert(L, 1, 484);
    ListInsert(L, 2, 2659);
    ListInsert(L, 3, 358458);
    ListInsert(L, 2, 274);
    int DelElem;
    ListDelete(L, 3, DelElem);
    printf("删除的元素为：%d\n", DelElem);

    int Elem = GetElem(L, 2);
    printf("按位获取%d的元素值为：%d\n", 2, Elem);

    int i = LocateElem(L, 358458);
    printf("按值获取%d元素的位序为：%d\n", 358458, i);
    return 0;
}