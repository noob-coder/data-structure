#include <stdio.h>
#include <stdlib.h>


/**
 * 链式栈（单链表的限定操作）
 * 头节点为栈顶端
 */
typedef struct SNode {
    int data; // 数据域
    SNode *next; // 指针域
} SNode, *LinkStack;

int Length(LinkStack);

bool Empty(LinkStack);

void Print(LinkStack S);

/**
 * 初始化链式栈
 * @param S
 */
bool InitStack(LinkStack &S) {
    S = (SNode *) malloc(sizeof(SNode));
    if (S == nullptr) {
        return false;
    }
    // 带头节点方式，头节点数据域存储链表长度
    S->data = 0;
    S->next = nullptr;

    return true;
}

/**
 * 入栈（头插法）
 * @param S
 * @param e
 * @return
 */
bool Push(LinkStack &S, int e) {
    SNode *n = (SNode *) malloc(sizeof(SNode));

    if (n == nullptr) {
        return false;
    }

    n->data = e;
    n->next = S->next;
    S->next = n;

    S->data++;

    Print(S);
    return true;
}

/**
 * 出栈
 * @param S
 * @param e
 * @return
 */
bool Pop(LinkStack &S, int &e) {
    if (Empty(S)) {
        printf("【出栈】空栈！\n");
        return false;
    }

    SNode *n = S->next;
    e = n->data;
    S->next = n->next;
    S->data--;
    free(n);
    Print(S);
    return true;
}

/**
 * 访问栈顶（不删除节点）
 * @param S
 * @param e
 * @return
 */
bool GetTop(LinkStack &S, int &e) {
    if (Empty(S)) {
        printf("【GetTop】空栈！\n");
        return false;
    }
    e = S->next->data;
    return true;
}

/**
 * 头节点数据存储长度    O(1)
 * 无需循环遍历再计算
 * @param S
 * @return
 */
int Length(LinkStack S) {
    return S->data;
}

bool Empty(LinkStack S) {
    // 不带头节点判断方式：
    //return S == nullptr;

    // 通过头节点长度信息判断：
    //return Length(S) == 0;

    // 带头节点方式：
    return S->next == nullptr;
}

/**
 * 打印
 * 如果没有设置长度数据，循环遍历用while
 * @param S
 */
void Print(LinkStack S) {
    int l = Length(S);
    printf("------ 打印开始 ------\n");
    SNode *n = S->next;
    for (int i = 0; i < l; i++) {
        printf("位序：%d\t 值：%d\t 指针：%p\n", i + 1, n->data, n);
        n = n->next;
    }
    printf("------ 打印结束 ------\n");
}


int main() {
    LinkStack S;
    InitStack(S);

    int PopElem;

    Push(S, 2);
    Push(S, 9);
    Push(S, 3);
    Push(S, 47);
    Push(S, 748);
    Push(S, 496);
    Push(S, 246);
    Push(S, 548);
    Push(S, 666);
    Push(S, 0);
    Pop(S, PopElem);
    printf("【出栈】值：%d\n", PopElem);
    Push(S, 7979);

    return 0;
}