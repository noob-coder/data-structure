
#include <stdio.h>

#define MaxLength 10

/**
 * 静态顺序栈
 */
typedef struct {
    int data[MaxLength]; // 栈数据域
    int topIndex; // 栈顶索引下标
} SqStack;

int Length(SqStack);

bool Empty(SqStack);

void Print(SqStack);

void InitStack(SqStack &S) {
    // 不一定从-1开始，也可以从0开始，但入栈出栈操作有所差别
    S.topIndex = -1;
}

/**
 * 压栈
 * @param S
 * @return
 */
bool Push(SqStack &S, int e) {
    if (S.topIndex >= MaxLength - 1) {
        printf("【压栈】压入失败，栈满！\n");
        return false;
    }
    S.topIndex++;
    S.data[S.topIndex] = e;

    // 等价操作（先加后用）：
    S.data[++S.topIndex] = e;

    Print(S);
    return true;
}

/**
 * 出栈
 * @param S
 * @param e
 * @return
 */
bool Pop(SqStack &S, int &e) {
    if (Empty(S)) {
        printf("【出栈】出栈失败，空栈！\n");
        return false;
    }
    printf("【出栈】值：%d\t 指针：%p\n", S.data[S.topIndex], &S.data[S.topIndex]);

    //e = S.data[S.topIndex];
    //S.topIndex--;

    // 等价操作（先用后减）：
    e = S.data[S.topIndex--];

    Print(S);
    return true;
}

/**
 * 获取栈顶元素（访问，但不删除）
 * @param S
 * @param e
 * @return
 */
bool GetTop(SqStack &S, int &e) {
    if (Empty(S)) {
        printf("【GetTop】空栈！\n");
        return false;
    }
    e = S.data[S.topIndex];
    return true;
}

int Length(SqStack S) {
    return S.topIndex + 1;
}

bool Empty(SqStack S) {
    // 通过长度方式判断
    //return Length(S) == 0;

    // 通过初始值判断
    return S.topIndex == -1;
}

void Print(SqStack S) {
    printf("------ 打印开始 ------\n");
    if (Empty(S)) {
        printf("空栈！\n");
    }
    for (int i = 0; i <= S.topIndex; i++) {
        printf("位序：%d\t 值：%d\t 指针：%p\n", i + 1, S.data[i], &S.data[i]);
    }
    printf("栈长度：%d\n", Length(S));
    printf("------ 打印结束 ------\n");
}

int main() {
    SqStack S;
    InitStack(S);

    int PopElem;

    Push(S, 2);
    Push(S, 9);
    Push(S, 3);
    Push(S, 47);
    Push(S, 748);
    Push(S, 496);
    Push(S, 246);
    Push(S, 548);
    Push(S, 666);
    Push(S, 0);
    Pop(S, PopElem);
    printf("【出栈】值：%d\n", PopElem);
    Push(S, 7979);

    return 0;
}